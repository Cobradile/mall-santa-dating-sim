/*
** Matthew Suttinger & Emerald Nova (emeraldnovagames@gmail.com)
** 
** This work is licensed under a Attribution-NonCommercial 4.0 International License
** More info at: https://creativecommons.org/licenses/by-nc/4.0/legalcode
*/

#include "font.h"

//	Functions
void font_init(void)
{
	/*	Initialize Font
	*/
	
	//	Loop through 8x8 segments to load each character tile
	for(int i = 0; i < 40; i++)
	{
		//	Locate tile
		font_small_tile[i].x = i*8;
		font_small_tile[i].y = 0;
		font_small_tile[i].width = 8;
		font_small_tile[i].height = 8;
	}
	
	//	Write tile sheet to palette SID
	font_small_tile_SID = jo_sprite_add_tga_tileset(JO_ROOT_DIR, "FONT_S.TGA",
		JO_COLOR_Blue, font_small_tile, JO_TILE_COUNT(font_small_tile));
	//font_small_tile_SID = jo_sprite_add_tga(JO_ROOT_DIR, "FONT_S.TGA", JO_COLOR_Blue);
	
	return;
}

void bottom_center(int x, int y, int w, float scale, char * string)
{
	int initX = x;
	int fontdex;
	
	while(*(string))
	{
		//	Find the correct character
		fontdex = -1;
		if((*(string) == 'A') || (*(string) == 'a')) {
			fontdex = 0;
		}
		else if(*(string) == 'B' || *(string) == 'b') {
			fontdex = 1;
		}
		else if(*(string) == 'C' || *(string) == 'c') {
			fontdex = 2;
		}
		else if(*(string) == 'D' || *(string) == 'd') {
			fontdex = 3;
		}
		else if(*(string) == 'E' || *(string) == 'e') {
			fontdex = 4;
		}
		else if(*(string) == 'F' || *(string) == 'f') {
			fontdex = 5;
		}
		else if(*(string) == 'G' || *(string) == 'g') {
			fontdex = 6;
		}
		else if(*(string) == 'H' || *(string) == 'h') {
			fontdex = 7;
		}
		else if(*(string) == 'I' || *(string) == 'i') {
			fontdex = 8;
		}
		else if(*(string) == 'J' || *(string) == 'j') {
			fontdex = 9;
		}
		else if(*(string) == 'K' || *(string) == 'k') {
			fontdex = 10;
		}
		else if(*(string) == 'L' || *(string) == 'l') {
			fontdex = 11;
		}
		else if(*(string) == 'M' || *(string) == 'm') {
			fontdex = 12;
		}
		else if(*(string) == 'N' || *(string) == 'n') {
			fontdex = 13;
		}
		else if(*(string) == 'O' || *(string) == 'o') {
			fontdex = 14;
		}
		else if(*(string) == 'P' || *(string) == 'p') {
			fontdex = 15;
		}
		else if(*(string) == 'Q' || *(string) == 'q') {
			fontdex = 16;
		}
		else if(*(string) == 'R' || *(string) == 'r') {
			fontdex = 17;
		}
		else if(*(string) == 'S' || *(string) == 's') {
			fontdex = 18;
		}
		else if(*(string) == 'T' || *(string) == 't') {
			fontdex = 19;
		}
		else if(*(string) == 'U' || *(string) == 'u') {
			fontdex = 20;
		}
		else if(*(string) == 'V' || *(string) == 'v') {
			fontdex = 21;
		}
		else if(*(string) == 'W' || *(string) == 'w') {
			fontdex = 22;
		}
		else if(*(string) == 'X' || *(string) == 'x') {
			fontdex = 23;
		}
		else if(*(string) == 'Y' || *(string) == 'y') {
			fontdex = 24;
		}
		else if(*(string) == 'Z' || *(string) == 'z') {
			fontdex = 25;
		}
		else if(*(string) == '0') {
			fontdex = 26;
		}
		else if(*(string) == '1') {
			fontdex = 27;
		}
		else if(*(string) == '2') {
			fontdex = 28;
		}
		else if(*(string) == '3') {
			fontdex = 29;
		}
		else if(*(string) == '4') {
			fontdex = 30;
		}
		else if(*(string) == '5') {
			fontdex = 31;
		}
		else if(*(string) == '6') {
			fontdex = 33;
		}
		else if(*(string) == '7') {
			fontdex = 33;
		}
		else if(*(string) == '8') {
			fontdex = 34;
		}
		else if(*(string) == '9') {
			fontdex = 35;
		}
		else if(*(string) == '.') {
			fontdex = 36;
		}
		else if(*(string) == '!') {
			fontdex = 37;
		}
		else if(*(string) == '?') {
			fontdex = 38;
		}
		else if(*(string) == '\'') {
			fontdex = 39;
		}
		
		
		//	New line if off screen
		if(x + (font_small_tile[fontdex].width + scale) > w)
		{
			x = initX;
			if(fontdex >= 0) y += font_small_tile[fontdex].height + 2;
			else y += font_small_tile[23].height + 2;
		}
		
		//	Draw if font is in tileset
		if(fontdex >= 0)
		{
			jo_sprite_draw3D2(font_small_tile_SID + fontdex, x, y, 400);
			x += font_small_tile[fontdex].width + 1;
		}
		else x += font_small_tile[23].width + 1;
		string++;
	}
}

