#pragma once
#ifndef __PROLOGUE_H__
# define __PROLOGUE_H__

#include "game.h"

void load_scene1(Scene * scene);
void load_scene2(Scene * scene);
void scene2_yes(Scene * scene);
void scene2_no(Scene * scene);

#endif