#include "day2.h"

void load_scene3(Scene * scene)
{
    scene->size = 11;
    scene->lines[0].speaker = "";
    scene->lines[0].dialogue = "Tuesday - 1st December";
    scene->lines[0].background = 0;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "";
    scene->lines[1].dialogue = "You head straight to Santa's grotto, and you hear some shouting";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = -1;

    scene->lines[2].speaker = "Higher Up";
    scene->lines[2].dialogue = "You're drinking on the job? In front of the kids!";
    scene->lines[2].background = 1;
    scene->lines[2].sprites[1] = -1;

    scene->lines[3].speaker = "You";
    scene->lines[3].dialogue = "Oh no... Please no...";
    scene->lines[3].background = 1;
    scene->lines[3].sprites[1] = -1;

    scene->lines[4].speaker = "Santa";
    scene->lines[4].dialogue = "I... hic... am fine! Leave me alone!";
    scene->lines[4].background = 1;
    scene->lines[4].sprites[1] = -1;

    scene->lines[5].speaker = "";
    scene->lines[5].dialogue = "You get to the grotto and see that it's a different Santa than yesterday";
    scene->lines[5].background = 1;
    scene->lines[5].sprites[1] = -1;

    scene->lines[6].speaker = "You";
    scene->lines[6].dialogue = "Whew! It's not him...";
    scene->lines[6].background = 1;
    scene->lines[6].sprites[1] = -1;

    scene->lines[7].speaker = "Higher Up";
    scene->lines[7].dialogue = "You are clearly intoxicated!";
    scene->lines[7].background = 1;
    scene->lines[7].sprites[1] = -1;

    scene->lines[8].speaker = "Higher Up";
    scene->lines[8].dialogue = "You're fired! Get out of here!";
    scene->lines[8].background = 1;
    scene->lines[8].sprites[1] = -1;

    scene->lines[9].speaker = "";
    scene->lines[9].dialogue = "The Santa leaves the grotto. He's a bit of a mess, but looks aren't too bad.";
    scene->lines[9].background = 1;
    scene->lines[9].sprites[1] = -1;

    scene->lines[10].speaker = "";
    scene->lines[10].dialogue = "He looks distressed. Maybe I should talk to him.";
    scene->lines[10].background = 1;
    scene->lines[10].sprites[1] = -1;

    scene->has_question = true;
    scene->optionCount = 2;
    scene->questionString = "Do you want to talk to him?";
    scene->answers[0] = "Sure, he seems alright.";
    scene->answers[1] = "Nah, he's a lost cause...";
    scene->lines[11].background = 1;
    scene->lines[11].sprites[1] = -1;
}

void load_scene4(Scene * scene)
{
}

void scene3_yes(Scene * scene)
{
    scene->size = 4;
    scene->lines[0].speaker = "You";
    scene->lines[0].dialogue = "Hey, are you okay?";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "Santa";
    scene->lines[1].dialogue = "Do I... hic... look okay?";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = -1;

    scene->lines[2].speaker = "Santa";
    scene->lines[2].dialogue = "I lost my job, and my rent is due next week!";
    scene->lines[2].background = 1;
    scene->lines[2].sprites[1] = -1;

    scene->lines[3].speaker = "Santa";
    scene->lines[3].dialogue = "It's over! I'm finished!";
    scene->lines[3].background = 1;
    scene->lines[3].sprites[1] = -1;

    scene->has_question = true;
    scene->optionCount = 3;
    scene->questionString = "What do you want to do?";
    scene->answers[0] = "Comfort him";
    scene->answers[1] = "Take his drink";
    scene->answers[2] = "Leave him alone";
    scene->lines[4].background = 1;
    scene->lines[4].sprites[1] = -1;
}

void scene3_take(Scene * scene)
{
    scene->size = 4;
    scene->lines[0].speaker = "You";
    scene->lines[0].dialogue = "Maybe I should take that";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "Santa";
    scene->lines[1].dialogue = "No! hic... it's mine!";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = -1;

    scene->lines[2].speaker = "You";
    scene->lines[2].dialogue = "It's for the best";
    scene->lines[2].background = 1;
    scene->lines[2].sprites[1] = -1;

    scene->lines[3].speaker = "Santa";
    scene->lines[3].dialogue = "No!";
    scene->lines[3].background = 1;
    scene->lines[3].sprites[1] = -1;

    scene->lines[4].speaker = "You";
    scene->lines[4].dialogue = "Whatever...";
    scene->lines[4].background = 1;
    scene->lines[4].sprites[1] = -1;

    scene->has_question = false;
}

void scene3_comfort(Scene * scene)
{
    scene->size = 4;
    scene->lines[0].speaker = "You";
    scene->lines[0].dialogue = "Do you need any help?";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "You";
    scene->lines[1].dialogue = "Do you need a place to crash?";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = -1;

    scene->lines[2].speaker = "Santa";
    scene->lines[2].dialogue = "Nah, I got my brother's place to crash in for now.";
    scene->lines[2].background = 1;
    scene->lines[2].sprites[1] = -1;

    scene->lines[3].speaker = "Santa";
    scene->lines[3].dialogue = "Thanks, anyway.";
    scene->lines[3].background = 1;
    scene->lines[3].sprites[1] = -1;

    scene->lines[4].speaker = "Ollie";
    scene->lines[4].dialogue = "The name's ollie, by the way. Nice to meet you.";
    scene->lines[4].background = 1;
    scene->lines[4].sprites[1] = -1;

    scene->lines[5].speaker = "You";
    scene->lines[5].dialogue = "Nice to meet you, too";
    scene->lines[5].background = 1;
    scene->lines[5].sprites[1] = -1;

    scene->lines[6].speaker = "You";
    scene->lines[6].dialogue = "You need to get off the drink";
    scene->lines[6].background = 1;
    scene->lines[6].sprites[1] = -1;

    scene->lines[7].speaker = "You";
    scene->lines[7].dialogue = "It's just making things worse for you";
    scene->lines[7].background = 1;
    scene->lines[7].sprites[1] = -1;

    scene->lines[8].speaker = "Ollie";
    scene->lines[8].dialogue = "I guess... I'll try and ease off of it";
    scene->lines[8].background = 1;
    scene->lines[8].sprites[1] = -1;

    scene->lines[9].speaker = "Ollie";
    scene->lines[9].dialogue = "For you, anyway.";
    scene->lines[9].background = 1;
    scene->lines[9].sprites[1] = -1;

    scene->has_question = false;
}

void scene3_no(Scene * scene)
{
    scene->size = 2;
    scene->lines[0].speaker = "You";
    scene->lines[0].dialogue = "Nah, he's not worth the effort.";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "";
    scene->lines[1].dialogue = "You leave the mall";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = -1;

    scene->has_question = false;
}