#include "prologue.h"

void load_scene1(Scene * scene)
{
    scene->size = 9;
    scene->lines[0].speaker = "";
    scene->lines[0].dialogue = "Monday - 30th November - Ontario, Canada";
    scene->lines[0].background = 0;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "You";
    scene->lines[1].dialogue = "*Yawn*";
    scene->lines[1].background = 0;
    scene->lines[1].sprites[1] = -1;

    scene->lines[2].speaker = "You";
    scene->lines[2].dialogue = "Ugh, where... ?";
    scene->lines[2].background = 0;
    scene->lines[2].sprites[1] = -1;

    scene->lines[3].speaker = "You";
    scene->lines[3].dialogue = "Oh yeah, that's right.";
    scene->lines[3].background = 0;
    scene->lines[3].sprites[1] = -1;

    scene->lines[4].speaker = "Monologue";
    scene->lines[4].dialogue = "A few days ago, I had to move to Canada for work.";
    scene->lines[4].background = 0;
    scene->lines[4].sprites[1] = -1;

    scene->lines[5].speaker = "Monologue";
    scene->lines[5].dialogue = "I have no friends or family here, so I'm all alone.";
    scene->lines[5].background = 0;
    scene->lines[5].sprites[1] = -1;

    scene->lines[6].speaker = "Monologue";
    scene->lines[6].dialogue = "Been difficult settling in to say the least. Still calling my parents and friends from time to time, so there is that.";
    scene->lines[6].background = 0;
    scene->lines[6].sprites[1] = -1;

    scene->lines[7].speaker = "Monologue";
    scene->lines[7].dialogue = "I should probably go Christmas shopping";
    scene->lines[7].background = 0;
    scene->lines[7].sprites[1] = -1;

    scene->lines[8].speaker = "Monologue";
    scene->lines[8].dialogue = "Been so preoccupied with the moving that I forgot all about that";
    scene->lines[8].background = 0;
    scene->lines[8].sprites[1] = -1;

    scene->has_question = false;
}

void load_scene2(Scene * scene)
{
    loadSprites();
    scene->size = 13;
    
    scene->lines[0].speaker = "You";
    scene->lines[0].dialogue = "Okay, I got something for my parents.";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = -1;

    scene->lines[1].speaker = "You";
    scene->lines[1].dialogue = "Got a book for my brother";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = -1;

    scene->lines[2].speaker = "You";
    scene->lines[2].dialogue = "Now to get something for my sister...";
    scene->lines[2].background = 1;
    scene->lines[2].sprites[1] = -1;

    scene->lines[3].speaker = "";
    scene->lines[3].dialogue = "You walk around trying to find a perfume shop or something";
    scene->lines[3].background = 1;
    scene->lines[3].sprites[1] = -1;

    scene->lines[4].speaker = "";
    scene->lines[4].dialogue = "You end up in the middle of the mall";
    scene->lines[4].background = 1;
    scene->lines[4].sprites[1] = -1;

    scene->lines[5].speaker = "";
    scene->lines[5].dialogue = "You see Santa's Grotto";
    scene->lines[5].background = 1;
    scene->lines[5].sprites[1] = -1;

    scene->lines[6].speaker = "";
    scene->lines[6].dialogue = "You see a well built Santa, with broad shoulders. Muscles everywhere";
    scene->lines[6].background = 1;
    scene->lines[6].sprites[1] = -1;

    scene->lines[7].speaker = "";
    scene->lines[7].dialogue = "He has more abs than the real Santa has Raindeer guiding his sleigh!";
    scene->lines[7].background = 1;
    scene->lines[7].sprites[1] = -1;

    scene->lines[8].speaker = "You";
    scene->lines[8].dialogue = "Wow, where did they find this guy?";
    scene->lines[8].background = 1;
    scene->lines[8].sprites[1] = -1;

    scene->lines[9].speaker = "You";
    scene->lines[9].dialogue = "Aren't Santas supposed to be fat?";
    scene->lines[9].background = 1;
    scene->lines[9].sprites[1] = -1;

    scene->lines[10].speaker = "";
    scene->lines[10].dialogue = "The Santa catches you staring at him, he gets up from his seat and walks towards you.";
    scene->lines[10].background = 1;
    scene->lines[10].sprites[1] = -1;

    scene->lines[11].speaker = "Handsome Santa";
    scene->lines[11].dialogue = "Hello there! Do you...           want to see my workshop?";
    scene->lines[11].background = 1;
    scene->lines[11].sprites[1] = 0;

    scene->lines[12].speaker = "You";
    scene->lines[12].dialogue = "I... Uh...";
    scene->lines[12].background = 1;
    scene->lines[12].sprites[1] = 0;


    scene->has_question = true;
    scene->optionCount = 2;
    scene->questionString = "Do you want to go on a date with him?";
    scene->answers[0] = "YES!!!";
    scene->answers[1] = "Erm... I will think about it...";
    scene->lines[13].background = 1;
    scene->lines[13].sprites[1] = 0;

    load_background(scene->lines[0].background);
}

void scene2_yes(Scene * scene)
{
    scene->size = 1;
    scene->lines[0].speaker = "Handsome Santa";
    scene->lines[0].dialogue = "I knew you would! Meet me here on Saturday";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = 0;
    scene->has_question = false;
}

void scene2_no(Scene * scene)
{
    scene->size = 3;
    scene->lines[0].speaker = "Handsome Santa";
    scene->lines[0].dialogue = "Not ready for this yet? That's okay.";
    scene->lines[0].background = 1;
    scene->lines[0].sprites[1] = 0;

    scene->lines[1].speaker = "Handsome Santa";
    scene->lines[1].dialogue = "Santa needs to fill his sack first, anyway";
    scene->lines[1].background = 1;
    scene->lines[1].sprites[1] = 0;

    scene->lines[2].speaker = "";
    scene->lines[2].dialogue = "He gives you a wink as he goes back to his grotto";
    scene->lines[2].background = 1;
    scene->lines[2].sprites[1] = -1;
    scene->has_question = false;
}