#pragma once
#ifndef __DAY2_H__
# define __DAY2_H__

#include "game.h"

void load_scene3(Scene * scene);
void load_scene4(Scene * scene);
void scene3_yes(Scene * scene);
void scene3_comfort(Scene * scene);
void scene3_take(Scene * scene);
void scene3_no(Scene * scene);

#endif