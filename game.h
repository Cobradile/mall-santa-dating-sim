#pragma once
#ifndef __GAME_H__
# define __GAME_H__

#include <jo/jo.h>

typedef struct {
	char * speaker;
    char * dialogue;
    int background;
    int sprites[3];
} Story;

typedef struct {
	Story lines[30];
    bool has_question;
    char * questionString;
    char * answers[3];
    int optionCount, size;
} Scene;

void update(void);
void new_scene();
void answer_question(int answer);
void my_draw(void);
void load_screen();
void load_background(int no);
void loadSprites();

#endif /* !__GAME_H__ */
/*
** END OF FILE
*/
