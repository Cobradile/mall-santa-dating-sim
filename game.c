#include "game.h"

Scene scene;
int currentState = 0;
int currentQuestion = 0;
int currentOption = 0;
// 0 = Title
// 1 = Normal
// 2 = Question

int currentLength = 0;
char sub[300];

int currentScene;
int currentLine;
int currentBG;
int currentSprites[3];

int timer = 0;
int cooldown = 10;

void    update(void)
{
    if (currentState == 1)
    {
        if (timer > cooldown)
        {
            if (currentLength >= strlen(scene.lines[currentLine].dialogue))
            {
                if (jo_is_pad1_key_pressed(JO_KEY_A))
                {
                    currentLine++;
                    currentLength = 0;
                    clean(sub);
                    if (currentLine >= scene.size)
                    {
                        if (scene.has_question) currentState = 2;
                        else if (currentQuestion > 0) handle_branch();
                        else
                        {
                            new_scene();
                        }
                    }

                    if (scene.lines[currentLine].background != currentBG)
                        load_background(scene.lines[currentLine].background);
                    timer = 0;
                }
            }
            else if (jo_is_pad1_key_pressed(JO_KEY_A)) currentLength += 2;
            else currentLength++;
        }
        else timer++; 
    }
    else if (currentState == 2)
    {
        if (timer > cooldown)
        {
            if (currentLength >= strlen(scene.questionString))
            {
                if (jo_is_pad1_key_pressed(JO_KEY_DOWN))
                {
                    if (currentOption < scene.optionCount - 1) currentOption++;
                    else currentOption = 0;
                    timer = 0;
                }
                else if (jo_is_pad1_key_pressed(JO_KEY_UP))
                {
                    if (currentOption > 0) currentOption--;
                    else currentOption = scene.optionCount - 1;
                    timer = 0;
                }
                else if (jo_is_pad1_key_pressed(JO_KEY_START))
                {
                    clean(sub);
                    currentLength = 0;
                    answer_question(currentOption);
                    timer = 0;
                }
            }
            else if (jo_is_pad1_key_pressed(JO_KEY_A)) currentLength += 2;
            else currentLength++;
        }
        else timer++;
    }
    else if (currentState != 3)
    {
        if (jo_is_pad1_key_pressed(JO_KEY_START))
        {
            jo_clear_background(JO_COLOR_Black);
            jo_clear_screen();
            load_screen();
            load_scene1(&scene);
            load_background(scene.lines[0].background);
            currentState = 1;
        }
    }
}

void new_scene()
{
    load_screen();
    currentLine = 0;
    currentQuestion = 0;
    currentState = 1;
    switch(currentScene)
    {
        case 0:
            currentScene = 1;
        case 1:
            currentScene = 2;
            load_scene2(&scene);
            break;
        case 2:
            currentScene = 3;
            load_scene3(&scene);
            break;
        case 3:
            jo_clear_background(JO_COLOR_Black);
            jo_clear_screen();
            currentState = 3;
            break;
    }
}

void answer_question(int answer)
{
    switch(currentScene)
    {
        case 2:
            if (answer == 0) scene2_yes(&scene);
            else scene2_no(&scene);
            break;
        case 3:
            if (currentQuestion == 0)
            {
                if (answer == 0)
                {
                    scene3_yes(&scene);
                    currentQuestion = 1;
                }
                else
                {
                    scene3_no(&scene);
                    currentQuestion = 0;
                }
            }
            else
            {
                currentQuestion = 2;
                if (answer == 0) scene3_comfort(&scene);
                else if (answer == 1) scene3_take(&scene);
                else
                {
                    scene3_no(&scene);
                    currentQuestion = 0;
                }
            }
            break;
    }
    currentState = 1;
    currentLine = 0;
}

void handle_branch()
{
    switch(currentScene)
    {
        case 3:
            if (currentQuestion == 2)
                if (currentOption == 1)
                {
                    currentLine = 0;
                    currentQuestion = -1;
                    scene3_no(&scene);
                }
                else new_scene();
                break;
    }
}

void load_screen()
{jo_clear_background(JO_COLOR_Black);
    currentState = -1;
    load_background(0);
}

void clean(char *var) {
    int i = 0;
    while(i < 300) {
        var[i] = '\0';
        i++;
    }
}

void			    my_draw(void)
{
    switch(currentState)
    {
        case -1:
            bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4 + 15, JO_TV_WIDTH - 20, 2, "Loading...");
            break;
        case 1:
            bottom_center(5, JO_TV_HEIGHT - JO_TV_HEIGHT_4 -10, JO_TV_WIDTH - 10, 1, scene.lines[currentLine].speaker);
            
            if (currentLength >= strlen(scene.lines[currentLine].dialogue)) bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4, JO_TV_WIDTH - 10, 1, scene.lines[currentLine].dialogue);
            else bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4, JO_TV_WIDTH - 10, 1, strncpy(sub, scene.lines[currentLine].dialogue, currentLength));

            if (scene.lines[currentLine].sprites[1] >= 0)
            {
                jo_sprite_change_sprite_scale(1.5);
                jo_sprite_draw3D(40 + scene.lines[currentLine].sprites[1], 0, -25, 500);
                jo_sprite_change_sprite_scale(1);
            }
            break;
        case 2:
            if (currentLength >= strlen(scene.questionString))
            {
                bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4 -10, JO_TV_WIDTH - 10, 1, scene.questionString);
                for (int c = 0; c < scene.optionCount; c++)
                {
                    if (c == currentOption) 
                    {
                        bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4 + (15 + (10 * c)), JO_TV_WIDTH, 1, "-");
                        bottom_center(20, JO_TV_HEIGHT - JO_TV_HEIGHT_4 + (15 + (10 * c)), JO_TV_WIDTH, 1, scene.answers[c]);
                    }
                    else bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4 + (15 + (10 * c)), JO_TV_WIDTH, 1, scene.answers[c]);
                }
            }
            else bottom_center(10, JO_TV_HEIGHT - JO_TV_HEIGHT_4 -10, JO_TV_WIDTH - 10, 1, strncpy(sub, scene.questionString, currentLength));
            if (scene.lines[currentLine].sprites[1] >= 0)
            {
                jo_sprite_change_sprite_scale(1.5);
                jo_sprite_draw3D(40 + scene.lines[currentLine].sprites[1], 0, -25, 500);
                jo_sprite_change_sprite_scale(1);
            }
            break;
        case 3:
            jo_clear_background(JO_COLOR_Black);
            bottom_center(10, 20, JO_TV_WIDTH, 3, "End of Demo");
            bottom_center(10, 40, JO_TV_WIDTH, 3, "This was all we wrote so far...");
            bottom_center(10, 60, JO_TV_WIDTH, 3, "Hope you enjoyed!");
            bottom_center(10, 80, JO_TV_WIDTH, 3, "Happy Holidays");
            bottom_center(10, 100, JO_TV_WIDTH, 3, "and a Happy New Year!");
            break;
        default:
            jo_clear_background(JO_COLOR_Black);
            // jo_printf(5, 5, "Kriss Mingle");
            // jo_printf(5, 10, "Graphics By - Dreamcast Freak");
            // jo_printf(5, 12, "Music By - Sharkhat");
            // jo_printf(5, 14, "Programming By - Cobra!");
            // jo_printf(5, 20, "Press Start");
            bottom_center(10, 20, JO_TV_WIDTH, 3, "Kriss Mingle");
            bottom_center(10, 40, JO_TV_WIDTH, 1, "Graphics By - Dreamcast Freak");
            bottom_center(10, 50, JO_TV_WIDTH, 1, "Music By - Sharkhat");
            bottom_center(10, 60, JO_TV_WIDTH, 1, "Programming By - Cobra!");
            
            bottom_center(10, 200, JO_TV_WIDTH, 1, "Version 0.1");
    }

}

void load_background(int no)
{
    jo_clear_background(JO_COLOR_Black);
    if (no == 1)
    {
        jo_img bg;
        bg.data = JO_NULL;
        jo_tga_loader(&bg, "BG", "BG.TGA", JO_COLOR_Transparent);
		jo_audio_play_cd_track(3, 3, 1);
        jo_set_background_sprite(&bg, 0, 0);
        int c;
        for (c = 0; c < JO_TV_HEIGHT_4 / 1.5; c++)
            jo_draw_background_line(1.5, JO_TV_HEIGHT_2 - (20 - c), JO_TV_WIDTH_2 + 27, 
            JO_TV_HEIGHT_2 - (20 - c), JO_COLOR_DarkGray);
        jo_zoom_background(0.5999);
        jo_free_img(&bg);   
    }
    else
    {
		jo_audio_stop_cd();
        jo_zoom_background(1);
        int c;
        for (c = 0; c < JO_TV_HEIGHT_4; c++)
            jo_draw_background_line(3, JO_TV_HEIGHT - JO_TV_HEIGHT_4 - (10 - c), JO_TV_WIDTH - 10, 
            JO_TV_HEIGHT - JO_TV_HEIGHT_4 - (10 - c), JO_COLOR_DarkGray);
    }
    
    currentBG = no;
}

void loadSprites()
{
    currentSprites[1] = jo_sprite_add_tga("TEX", "CHAD.TGA", JO_COLOR_Transparent);
}

/*
** END OF FILE
*/
